#!/usr/bin/python3
#-*- coding: utf-8 -*-


import serial


def format_serial_command(ser, chan, command, setting):
    """Format and send to `ser` device a command and its
    setting, on the specified channel (1 or 2)"""
    if chan == 1:
        comm = "b" + command + str(setting) + '\n'
    if chan == 2:
        comm = "d" + command + str(setting) + '\n'
    
    ser.write(bytes(comm, "utf-8"))


class Device:
    def __init__(self, port):
        """Class for a Serial port-connected FY322x device"""
        self.port = port
        self.baud = 9600
        self.timeout = 0.5
        self.ser = None
        self.dev_name = ""
    
    def open(self):
        """Open the serial port communication, and get the device name"""
        self.ser = serial.Serial(port=self.port, baudrate=self.baud, timeout=self.timeout)
        self.ser.write(b'a\n')
        self.dev_name = self.ser.readline().decode()
    
    def close(self):
        """Close the serial port communication"""
        self.ser.close()
    
    def set_frequency(self, chan, freq):
        """Set the signal frequency on channel 1 or 2"""
        freq = int(100 * freq) # conversion between Hz to cHz
        
        format_serial_command(self.ser, chan, 'f', freq)
    
    def set_amplitude(self, chan, ampl):
        """Set the signal amplitude on channel 1 or 2"""
        format_serial_command(self.ser, chan, 'a', ampl)
    
    def set_offset(self, chan, offset):
        """Set the signal DC offset on channel 1 or 2"""
        format_serial_command(self.ser, chan, 'o', offset)
    
    def set_waveform(self, chan, wave):
        """Set the signal waveform on channel 1 or 2 (0 to 16)"""
        format_serial_command(self.ser, chan, 'w', wave)
    
    def set_duty_cycle(self, chan, duty):
        """Set the signal duty cycle on channel 1 or 2 (0.0 to 1.0)"""
        duty = int(1000 * duty)
        format_serial_command(self.ser, chan, 'd', duty)
    
    def set_phase(self, phase):
        """Set the phase on channel 2, relative to channel 1 (degrees)"""
        format_serial_command(self.ser, 2, 'p', phase)
