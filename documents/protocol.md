FY3200S Series Communication Protocol
=====================================

*Adapted from the original manufacturer's manual rev. 1.2 (2015/09/11)*
-----------------------------------------------------------------------


This is the protocol specification for communicating with FY3200S series digital arbitrary function generators, from a host computer.


Summary
-------

The function generator is connected to the host computer through a serial connection, at a fixed **9600 bps** baudrate.
The instructions must be written in lowercase characters and numbers from 0 to 9.
Each command ends with the line break character ("`\n` or `0x0a`). It has a maximum of **15 characters** (including the line break character).

`a` commands
------------

### `a + 0x0a`

Returns the model of the machine : `FY3202S`, `FY3205S`, `FY3208S`, `FY3210S`, or `FY3224S`.

`b` commands
------------

These commands control the main waveform.

### `bw* + 0x0a`

Sets the type of the main waveform.

`*` represents a digit which allows to chose the waveform, ie. :

 * 0 for a sine wave
 * 1 for a triangle or sawtooth wave
 * 2 for a square wave
 
### `bfxxxxxxxxx + 0x0a`

Sets the frequency of the main waveform.
The frequency (in hertz) is given instead of `xxxxxxxxx`, on 9 digits.

For example:
* `bf100000000` means the frequency is set to 1MHz.
* `bf000123456` means the frequency is set to 1.23456kHz.
* `bf000000001` means the frequency is set to 0.01Hz.
 
### `baxx.x + 0x0a`

Sets the amplitude of the main waveform.
The amplitude (in volts) is given instead of `xx.x`.

For example:

* `ba12.3` means the amplitude is set to 12.3V.
* `ba0.3` means the amplitude is set to 0.3V.
 
### `boxxx.x + 0x0a`

Sets the DC offset of the main waveform.
The offset (in volts) is given instead of `xxx.x`.

For example:

* `bo2.3` means the offset is set to 2.3V.
* `bo-2.3` means the offset is set to -2.3V.

### `bdxx + 0x0a`

Sets the duty cycle of the main waveform.
The duty cycle (in percent) is given instead of `xx`.

For example:

* `bd51` means the duty cycle is set to 51%.

### `btxx + 0x0a`

Sets the sweep time of the main waveform.
The sweep time (in seconds) is given instead of `xx`.

* `bt51` means the sweep time is set to 51s.

### `bsx + 0x0a`

Saves current parameters (Frequency, Duty Cycle, Waveform etc.) to specific position (0 to 9).
Above `x` shows the saving positions represented by 1 digit.

For example: 

* `bs0` means saving to Position 0. The position is for default value. In other words, parameters saved in this position will be loaded automatically at start-up.
* `bs1` means saving to Position 1. The position is also for start value for sweep function.
* `bs2` means saving to Position 2. The position is also for end value for sweep function.

### `blx + 0x0a`

Loads parameters from specific positions (0 to 9).
Above `x` shows the saving positions represented by 1 digit.

For example:

* `bl3` means loading parameters (Frequency, Duty Cycle, Waveform etc.) from position 3.

### `brx + 0x0a`

Starts and stops sweep.

* `br1` to start sweep.
* `br0` to pause sweep.

### `bmx + 0x0a`

Sets the sweep mode.

* `bm0` for linear sweep mode.
* `bm1` for logarithmic sweep mode.

### `bbxxxxxxxxx + 0x0a`

Sets the sweep beginning frequency.
See `bfxxxxxxxxx + 0x0a` for syntax details.

### `bbxxxxxxxxx + 0x0a`

Sets the sweep ending frequency.
See `bfxxxxxxxxx + 0x0a` for syntax details.

`d` commands
------------

These commands control the secondary waveform.

### `dw* + 0x0a`

Sets the type of the secondary waveform.

`*` represents a digit which allows to chose the waveform, ie. :

 * 0 for a sine wave
 * 1 for a triangle or sawtooth wave
 * 2 for a square wave
 
### `dfxxxxxxxxx + 0x0a`

Sets the frequency of the secondary waveform.
The frequency (in hertz) is given instead of `xxxxxxxxx`, on 9 digits.

For example:
* `df100000000` means the frequency is set to 1MHz.
* `df000123456` means the frequency is set to 1.23456kHz.
* `df000000001` means the frequency is set to 0.01Hz.
 
### `daxx.x + 0x0a`

Sets the amplitude of the secondary waveform.
The amplitude (in volts) is given instead of `xx.x`.

For example:

* `da12.3` means the amplitude is set to 12.3V.
* `da0.3` means the amplitude is set to 0.3V.
 
### `doxxx.x + 0x0a`

Sets the DC offset of the secondary waveform.
The offset (in volts) is given instead of `xxx.x`.

For example:

* `do2.3` means the offset is set to 2.3V.
* `do-2.3` means the offset is set to -2.3V.

### `ddxx + 0x0a`

Sets the duty cycle of the secondary waveform.
The duty cycle (in percent) is given instead of `xx`.

For example:

* `dd51` means the duty cycle is set to 51%.

### `dpxxx + 0x0a`

Sets the phase (relative to the main waveform) of the secondary waveform.
The phase (in degrees) is given instead of `xxx`.

For example:

* `dp123` sets the phase of subsidiary waveform to 123°.
* `dp45` sets the phase of subsidiary waveform to 45°.
