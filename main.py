from device import *
import serial.tools.list_ports
import time

# list devices
ports = serial.tools.list_ports.comports(include_links=True)
print("Active serial ports :")
for prt in ports:
    print(" * " + prt.device + " - " + prt.description)

# tests
fg = Device("/dev/ttyUSB0")
fg.open()
print(fg.dev_name)
time.sleep(0.2)
fg.set_frequency(1, 4321)
time.sleep(0.2)
fg.set_frequency(2, 1234)
time.sleep(0.2)
fg.set_waveform(1, 3)
time.sleep(0.2)
fg.set_amplitude(1, 0.5)
time.sleep(0.2)
fg.set_duty_cycle(2, 0.5)
fg.close()
