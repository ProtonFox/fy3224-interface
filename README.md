# FY3224-Interface

A new open API and software for controlling the FY3224 function generator. It is designed as a replacement of the original software, which does not work on Linux and has interface issues.

This project will not receive any further updates as I sold my unit. However, this project remains free for anyone to use it.
